#include <iostream>
#include <cstdio>

#include "MessageSystem.h"
#include "resource/XML.h"
#include "resource/XMLInterface.h"
#include "resource/Registry.h"
#include "resource/ArgvParser.h"

#include "math/Vector.h"
#include "math/Quaternion.h"

int main(int __attribute__((unused)) argc, const char *argv[]) {
    using namespace Skeleton;

    auto configXML = Resource::Registry::get<Resource::XML>("data/config.xml");
    Resource::Registry::alias<Resource::XML>("data/config.xml", "config");

    Resource::ArgvParser().parse(configXML, argv);

    //std::cout << configXML->doc().first_child().first_child().first_element_by_path("/config/runtime") << std::endl;

    configXML->doc().save(std::cout);

    return 0;
}

#if 0

#include "config/ArgvParser.h"
#include "config/Parser.h"
#include "config/Tree.h"

#define DATA_PATH "data/"

void usage(std::shared_ptr<Config::Tree> ctree, const char *argv0) {
    std::cerr << "Usage: " << argv0 << " [arguments]" << std::endl;
    std::cerr << "Arguments: " << std::endl;

    auto argsnode = ctree->node("arguments.long_form");

    for(int i = 0; i < argsnode->childCount(); i ++) {
        auto argnode = argsnode->child(i);
        std::fprintf(stderr, "\t--%-20s %s\n", argnode->name().c_str(),
            argnode->child("desc")->get(
                std::string("[no description]")).c_str());
    }
}

int main(int argc, char *argv[]) {
    using namespace Skeleton;

    /* construct config tree */
    std::shared_ptr<Config::Tree> ctree = Config::Tree::instance();
    Config::Parser parser(ctree);
    parser.parseFile(DATA_PATH "/skeleton.config");
    parser.parseFile(DATA_PATH "/arguments.config");

    Config::ArgvParser argvparser(argv);
    argvparser.parse();

    if(ctree->node("runtime.help")->get<bool>(false)) {
        usage(ctree, argv[0]);
        return 0;
    }

    MessageSystem::setLogFile(ctree->node("log.filename")->get<std::string>());

    // insert main code here

    ctree->destroy();

    return 0;
}

#endif
