
#if 

// Why was this not in a namespace before? 
namespace Skeleton {
namespace Resource {

template <class T>
struct HasXMLInterfaceImpl 
{
private:
  
  template <class U>
  static MP::true_ test( typename U::XMLInterfaceType* );
  
  template <class U>
  static MP::false_ test(...);
  
public:
  using type = decltype( test<T>(0) );
};

template <class T>
struct HasXMLInterface : HasXMLInterfaceImpl<T>::type
{};

template <>
struct HasXMLInterface<int> : MP::false_
{};

template <>
struct HasXMLInterface<double> : MP::false_
{};

template <>
struct HasXMLInterface<bool> : MP::false_
{};


/* Lets get rid of some enable_ifs */

template <class T, class Ret = T
  , bool HasInter = HasXMLInterface<T>::value
>
struct Interface
{
  using None = Ret;
  static_assert(!HasInter, "Overload resolution check");
};

template <class T, class Ret>
struct Interface<std::vector<T>, Ret, false>
{
  using Vector = Ret;
};

template <class ...Args, class Ret>
struct Interface<std::tuple<Args...>, Ret, false>
{
  using Tuple = Ret;
};

template <class T, class Ret>
struct Interface<T, Ret, true>
{
  using Simple = Ret;
};

/* Now for tuple specializations */

template <std::size_t Index, typename T, typename Ret = void
    , bool AtStart = (Index == std::tuple_size<T>::value)>
struct TuplePos
{
  using Rest = Ret;
  
  static_assert(!AtStart, "Overload resolution check");
};

template <std::size_t Index, typename T, typename Ret>
struct TuplePos<Index, T, Ret, true>
{
  using Start = Ret;
};

}  // namespace Resource
}  // namespace Skeleton
