#ifndef SKELETON_RESOURCE__ARGV_PARSER_H
#define SKELETON_RESOURCE__ARGV_PARSER_H

#include "XML.h"

namespace Skeleton {
namespace Resource {

class ArgvParser {
public:
    void parse(std::shared_ptr<XML> configXML, const char **argv);
};

}  // namespace Resource
}  // namespace Skeleton

#endif
