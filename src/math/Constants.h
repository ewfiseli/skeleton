#ifndef SKELETON_MATH__CONSTANTS_H
#define SKELETON_MATH__CONSTANTS_H

#include <cmath>

namespace Skeleton {
namespace Math {

namespace Constants {

/* Used to represent numerical imprecision in floating-point results. */
const double Epsilon = 1e-7;

const double Pi = M_PI;

};

}  // namespace Math
}  // namespace Skeleton

#endif
