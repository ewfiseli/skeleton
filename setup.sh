#!/bin/bash

if [[ -z $1 ]]; then
    echo "Use: $0 namespace-name"
    exit 1
fi

#echo $1 | perl -pe 's/([A-Z]+)/substr($1, 0, 1) . lc(substr($1, 1))/eg' | perl -pe 'while($guard =~ s/([A-Z])([^:]*)$/'_' . lc($1) . $2/eg) {}'
GUARDNAME=$(echo $1 | perl -e 'while(<>){print uc}')
EXECNAME=$(echo $1 | perl -e 'while(<>){print lc}')


FILES="$(find src -name '*.h' -o -name '*.cpp' -o -name CMakeLists.txt) newclass.pl COPYING .gitignore .ycm_extra_conf.py"

echo $FILES

sed -i -e "s/SKELETON/$GUARDNAME/g" -e "s/Skeleton/$1/g" -e "s/skeleton/$EXECNAME/g" $FILES
