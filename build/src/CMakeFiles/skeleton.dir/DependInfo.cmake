# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/eric/tmp/skeleton/src/MessageSystem.cpp" "/home/eric/tmp/skeleton/build/src/CMakeFiles/skeleton.dir/MessageSystem.cpp.o"
  "/home/eric/tmp/skeleton/src/main.cpp" "/home/eric/tmp/skeleton/build/src/CMakeFiles/skeleton.dir/main.cpp.o"
  "/home/eric/tmp/skeleton/src/math/Matrix.cpp" "/home/eric/tmp/skeleton/build/src/CMakeFiles/skeleton.dir/math/Matrix.cpp.o"
  "/home/eric/tmp/skeleton/src/math/Quaternion.cpp" "/home/eric/tmp/skeleton/build/src/CMakeFiles/skeleton.dir/math/Quaternion.cpp.o"
  "/home/eric/tmp/skeleton/src/math/Vector.cpp" "/home/eric/tmp/skeleton/build/src/CMakeFiles/skeleton.dir/math/Vector.cpp.o"
  "/home/eric/tmp/skeleton/src/resource/ArgvParser.cpp" "/home/eric/tmp/skeleton/build/src/CMakeFiles/skeleton.dir/resource/ArgvParser.cpp.o"
  "/home/eric/tmp/skeleton/src/resource/Element.cpp" "/home/eric/tmp/skeleton/build/src/CMakeFiles/skeleton.dir/resource/Element.cpp.o"
  "/home/eric/tmp/skeleton/src/resource/File.cpp" "/home/eric/tmp/skeleton/build/src/CMakeFiles/skeleton.dir/resource/File.cpp.o"
  "/home/eric/tmp/skeleton/src/resource/Registry.cpp" "/home/eric/tmp/skeleton/build/src/CMakeFiles/skeleton.dir/resource/Registry.cpp.o"
  "/home/eric/tmp/skeleton/src/resource/XML.cpp" "/home/eric/tmp/skeleton/build/src/CMakeFiles/skeleton.dir/resource/XML.cpp.o"
  "/home/eric/tmp/skeleton/src/resource/XMLInterface.cpp" "/home/eric/tmp/skeleton/build/src/CMakeFiles/skeleton.dir/resource/XMLInterface.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "DEBUG"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/eric/tmp/skeleton/build/libs/pugixml/CMakeFiles/pugixml.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../src/."
  "../libs/pugixml"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
